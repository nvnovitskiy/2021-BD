# Большие данные
Выполненные лабораторные работы по курсу: "Большие данные"
____

# Лабораторные работы

 № Л/р | Название | Статус| Ссылка
 ----- |----------|-------|------
 1 | Introduction to Apache Spark |✅| [Ссылка](https://github.com/nvnovitskiy/big-data/tree/main/L1%20-%20Introduction%20to%20Apache%20Spark)
 2 | Reports with Apache Spark |✅| [Ссылка](https://github.com/nvnovitskiy/big-data/tree/main/L2%20-%20Reports%20with%20Apache%20Spark)
 3 | Stream processing with Apache Flink |✅| [Ссылка](https://github.com/nvnovitskiy/big-data/tree/main/L3%20-%20Stream%20processing%20with%20Apache%20Flink) 
 4 | ZooKeeper |✅| [Ссылка](https://github.com/nvnovitskiy/big-data/tree/main/L4%20-%20ZooKeeper)
